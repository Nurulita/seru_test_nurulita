package com.test.serufront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeruApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeruApplication.class, args);
	}

}
