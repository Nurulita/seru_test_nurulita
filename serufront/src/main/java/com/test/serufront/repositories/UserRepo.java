package com.test.serufront.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.serufront.models.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long>{
    @Query(value = "SELECT * FROM users WHERE Username = ?1 AND Password=?2", nativeQuery = true)
    List<User>getLogin(String Username, String Password);
    @Query(value = "SELECT * FROM   users WHERE is_delete = false",nativeQuery = true)
    List<User>getAllUser();
    
}
