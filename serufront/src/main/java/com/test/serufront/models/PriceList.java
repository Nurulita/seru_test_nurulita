package com.test.serufront.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "price_list")
public class PriceList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "code", length = 40)
    private String Code;

    @Column(name = "price")
    private Double Price;

    @ManyToOne
	@JoinColumn(name="year_id", insertable=false, updatable=false)
	public VehicleYear vehicle_year;
	
	@NotNull
	@Column(name="year_id")
	private Long Year_id;

    @ManyToOne
	@JoinColumn(name="model_id", insertable=false, updatable=false)
	public VehicleModel vehicle_model;
	
	@NotNull
	@Column(name="model_id")
	private Long Model_id;

    @NotNull
    @Column(name="created_by")
    private Long Created_by;

    @NotNull
    @Column(name="created_on")
    private Timestamp Created_on;

    @Nullable
    @Column(name="updated_by")
    private Long Updated_by;

    @Nullable
    @Column(name="updated_on")
    private Timestamp Updated_on;
    
    @Nullable
    @Column(name="deleted_by")
    private Long Deleted_by;

    @Nullable
    @Column(name="deleted_on")
    private Timestamp Deleted_on;

    @NotNull
    @Column(name="is_delete",columnDefinition = "boolean default false")
    private Boolean Is_delete;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double price) {
        Price = price;
    }

    public VehicleYear getVehicle_year() {
        return vehicle_year;
    }

    public void setVehicle_year(VehicleYear vehicle_year) {
        this.vehicle_year = vehicle_year;
    }

    public Long getYear_id() {
        return Year_id;
    }

    public void setYear_id(Long year_id) {
        Year_id = year_id;
    }

    public VehicleModel getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(VehicleModel vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public Long getModel_id() {
        return Model_id;
    }

    public void setModel_id(Long model_id) {
        Model_id = model_id;
    }

    public Long getCreated_by() {
        return Created_by;
    }

    public void setCreated_by(Long created_by) {
        Created_by = created_by;
    }

    public Timestamp getCreated_on() {
        return Created_on;
    }

    public void setCreated_on(Timestamp created_on) {
        Created_on = created_on;
    }

    public Long getUpdated_by() {
        return Updated_by;
    }

    public void setUpdated_by(Long updated_by) {
        Updated_by = updated_by;
    }

    public Timestamp getUpdated_on() {
        return Updated_on;
    }

    public void setUpdated_on(Timestamp updated_on) {
        Updated_on = updated_on;
    }

    public Long getDeleted_by() {
        return Deleted_by;
    }

    public void setDeleted_by(Long deleted_by) {
        Deleted_by = deleted_by;
    }

    public Timestamp getDeleted_on() {
        return Deleted_on;
    }

    public void setDeleted_on(Timestamp deleted_on) {
        Deleted_on = deleted_on;
    }

    public Boolean getIs_delete() {
        return Is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        Is_delete = is_delete;
    }
}
