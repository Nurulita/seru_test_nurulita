package com.test.serufront.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/brand")
public class VehicleBrandController {
    @GetMapping("/")
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("/vehiclebrand/index");
        return view;
    }
}
