package com.test.serufront.controllers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.test.serufront.models.User;
import com.test.serufront.repositories.UserRepo;

@Controller
@RequestMapping(value = "user")
public class UserController {
    @Autowired private UserRepo userrepo;

    @GetMapping(value = "index")
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("/home");
        List<User> user = this.userrepo.getAllUser();
        return view;

    }

    @GetMapping(value = "/register")
    public ModelAndView register(){
        ModelAndView view = new ModelAndView("/register");
        User user = new User();
        view.addObject("user", user);
        return view;
    }

    private static String bytesToHex(byte[] hash){
        StringBuilder hexString = new StringBuilder(2*hash.length);
        for(int i=0; i< hash.length; i++){
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    @PostMapping(value = "ceklogin")
    ModelAndView ceklogin(@ModelAttribute User users, BindingResult result, HttpSession sess){
        String redirect="";
        if(!result.hasErrors()){
            String username = (String) result.getFieldValue("Username");
            String password = (String) result.getFieldValue("Password");

            try{
                MessageDigest digest;
                digest = MessageDigest.getInstance("SHA-256");
                byte[] encodehash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
                String pass = bytesToHex(encodehash);

                List<User> getuser = this.userrepo.getLogin(username, pass);

                try{
                    sess.setAttribute("uid", getuser.get(0).getId());
                    sess.setAttribute("username", getuser.get(0).getUsername());
                    sess.setAttribute("fullname", getuser.get(0).getFullname());
                    System.out.println("Access Granted!");
                    if(getuser.get(0).getId()==null){
                        redirect = "redirect:/";
                    }else{
                        redirect = "redirect:/home";
                    }
                }catch(Exception e){
                    System.out.println("Access Denied!");
                    redirect = "redirect:/";
                }
            }catch(NoSuchAlgorithmException e){
                e.printStackTrace();
                redirect = "redirect:/";
            }
        }
        return new ModelAndView(redirect);
    }

    @PostMapping(value = "save")
    ModelAndView save(
        @ModelAttribute User users,
        BindingResult result) throws Exception
    {
        if(!result.hasErrors()){
            try {
                String pass = (String) result.getFieldValue("Password");
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                byte[] encodehash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));
                users.setPassword(bytesToHex(encodehash));
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                users.setCreated_by((long)1);
                users.setCreated_on(timestamp);
                users.setRole_id((long)2);
                users.setIs_delete(false);
                this.userrepo.save(users);

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } 
        }
        return new ModelAndView("redirect:/user/index");
    }

}
