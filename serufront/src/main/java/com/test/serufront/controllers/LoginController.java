package com.test.serufront.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/")
public class LoginController {
    
    @GetMapping(value = "/")
    public ModelAndView login(){
        ModelAndView view = new ModelAndView("/login");
        return view;
    }

}
