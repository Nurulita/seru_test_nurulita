CREATE TABLE teachers (
    id INTEGER primary key generated by default as identity,
    name VARCHAR(100),
    subject VARCHAR(50)
);

INSERT INTO teachers (name, subject) VALUES ('Pak Anton', 'Matematika');
INSERT INTO teachers (name, subject) VALUES ('Bu Dina', 'Bahasa Indonesia');
INSERT INTO teachers (name, subject) VALUES ('Pak Eko', 'Biologi');

CREATE TABLE classes (
    id INTEGER primary key generated by default as identity,
    name VARCHAR(50),
    teacher_id INTEGER,
    FOREIGN KEY (teacher_id) REFERENCES teachers(id)
);

INSERT INTO classes (name, teacher_id) VALUES ('Kelas 10A', 1);
INSERT INTO classes (name, teacher_id) VALUES ('Kelas 11B', 2);
INSERT INTO classes (name, teacher_id) VALUES ('Kelas 12C', 3);

CREATE TABLE students (
    id INTEGER primary key generated by default as identity,
    name VARCHAR(100),
    age INTEGER,
    class_id INTEGER,
    FOREIGN KEY (class_id) REFERENCES classes(id)
);

INSERT INTO students (name, age, class_id) VALUES ('Budi', 16, 1);
INSERT INTO students (name, age, class_id) VALUES ('Ani', 17, 2);
INSERT INTO students (name, age, class_id) VALUES ('Candra', 18, 3);

1. Tampilkan daftar siswa beserta kelas dan guru yang mengajar kelas tersebut.
select 
s.name as nama_siswa,
c.name as kelas,
t.name as nama_guru
from students s 
join classes c on s.class_id = c.id 
join teachers t on c.teacher_id = t.id 

2. Tampilkan daftar kelas yang diajar oleh guru yang sama.
select 
c.name as kelas,
t.name as nama_guru
from classes c  
join teachers t on c.teacher_id = t.id 
group by c.teacher_id, c.name, t.name;

select 
c.name as kelas,
t.name as nama_guru
from classes c  
join teachers t on c.teacher_id = t.id 
where t.name = 'Bu Dina';

3. buat query view untuk siswa, kelas, dan guru yang mengajar

create view view_school AS 
select
s.name as nama_siswa,
s.age  as usia,
c.name as kelas,
t.subject as pelajaran,
t.name as nama_guru
from students s 
inner join classes c on s.class_id = c.id 
inner join teachers t on c.teacher_id = t.id;

select * from view_school vs;
 
4. buat query yang sama tapi menggunakan store_procedure

CREATE OR REPLACE FUNCTION fun_school()
RETURNS TABLE (nama_siswa VARCHAR, usia INTEGER, kelas VARCHAR, pelajaran VARCHAR, nama_guru VARCHAR)
AS $$
BEGIN
    RETURN QUERY SELECT s.name, s.age, c.name, t.subject,t.name
                 FROM students s
                 inner join classes c on s.class_id = c.id 
				 inner join teachers t on c.teacher_id = t.id;
END;
$$ LANGUAGE plpgsql;
SELECT * FROM fun_school();

5. buat query input, yang akan memberikan warning error jika ada data yang sama pernah masuk

ALTER TABLE students 
ADD UNIQUE (name);
INSERT INTO students (name, age, class_id) VALUES ('Budi', 16, 1);

select * from students s;