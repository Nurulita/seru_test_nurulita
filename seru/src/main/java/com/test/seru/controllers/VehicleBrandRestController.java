package com.test.seru.controllers;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.seru.models.VehicleBrand;
import com.test.seru.repositories.VehicleBrandRepo;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin("*")
public class VehicleBrandRestController {
    @Autowired private VehicleBrandRepo vehiclebrandrepo;

    @GetMapping("/brand")
    public ResponseEntity<List<VehicleBrand>> getAllBrand(){
        try {
            List<VehicleBrand> vehiclebrand = this.vehiclebrandrepo.getAllBrand();
            return new ResponseEntity<List<VehicleBrand>>(vehiclebrand, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<VehicleBrand>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("brand/{id}")
    public ResponseEntity<?> getBrandById(@PathVariable("id") Long id){
        try {
            VehicleBrand vehiclebrand = this.vehiclebrandrepo.findById(id).orElse(null);
            if(vehiclebrand != null){
                return new ResponseEntity<VehicleBrand>(vehiclebrand, HttpStatus.OK);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Brand not found");
            }

        } catch (Exception e) {
            return new ResponseEntity<VehicleBrand>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/brand")
    public ResponseEntity<VehicleBrand> insertBrand(@RequestBody VehicleBrand vehiclebrand){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            vehiclebrand.setCreated_on(timestamp);
            vehiclebrand.setIs_delete(false);
            this.vehiclebrandrepo.save(vehiclebrand);
            return new ResponseEntity<VehicleBrand>(vehiclebrand, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<VehicleBrand>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/editBrand/{id}")
    public ResponseEntity<VehicleBrand> editBrandById(@RequestBody VehicleBrand brand, @PathVariable("id") Long id){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            brand.setId(id);
            brand.setUpdated_on(timestamp);
            brand.setIs_delete(false);
            this.vehiclebrandrepo.save(brand);
            return new ResponseEntity<VehicleBrand>(brand, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<VehicleBrand>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/brand/{id}")
    public ResponseEntity<?> deleteBrand(@PathVariable("id")long id){
        try {
            VehicleBrand vehiclebrand = this.vehiclebrandrepo.findById(id).orElse(null);
            if(vehiclebrand != null){
                this.vehiclebrandrepo.deleteById(id);
                return new ResponseEntity<VehicleBrand>(vehiclebrand, HttpStatus.OK);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Brand not found");
            }

        } catch (Exception e) {
            return new ResponseEntity<VehicleBrand>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/brandByName/{name}")
    public ResponseEntity<List<VehicleBrand>> getCategoryByName (
        @PathVariable("name") String name
    ) {
        try {
            List<VehicleBrand> brand = this.vehiclebrandrepo.getbrandByName(name);
            return new ResponseEntity<List<VehicleBrand>>(brand, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<List<VehicleBrand>>(HttpStatus.NO_CONTENT);
        }
    }
}
