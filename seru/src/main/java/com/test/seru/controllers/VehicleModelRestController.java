package com.test.seru.controllers;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.seru.models.VehicleModel;
import com.test.seru.repositories.VehicleModelRepo;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin("*")
public class VehicleModelRestController {
    @Autowired private VehicleModelRepo vehiclemodelrepo;

    @GetMapping("/model")
    public ResponseEntity<List<VehicleModel>> getAllModel(){
        try {
            List<VehicleModel> vehiclebrand = this.vehiclemodelrepo.getAllModel();
            return new ResponseEntity<List<VehicleModel>>(vehiclebrand, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<VehicleModel>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("model/{id}")
    public ResponseEntity<?> getModelById(@PathVariable("id") Long id){
        try {
            VehicleModel vehiclemodel = this.vehiclemodelrepo.findById(id).orElse(null);
            if(vehiclemodel != null){
                return new ResponseEntity<VehicleModel>(vehiclemodel, HttpStatus.OK);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Brand not found");
            }

        } catch (Exception e) {
            return new ResponseEntity<VehicleModel>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/model")
    public ResponseEntity<VehicleModel> insertModel(@RequestBody VehicleModel vehiclemodel){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            vehiclemodel.setCreated_on(timestamp);
            vehiclemodel.setIs_delete(false);
            this.vehiclemodelrepo.save(vehiclemodel);
            return new ResponseEntity<VehicleModel>(vehiclemodel, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<VehicleModel>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/editmodel/{id}")
    public ResponseEntity<VehicleModel> editModelById(@RequestBody VehicleModel model, @PathVariable("id") Long id){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            model.setId(id);
            model.setUpdated_on(timestamp);
            model.setIs_delete(false);
            this.vehiclemodelrepo.save(model);
            return new ResponseEntity<VehicleModel>(model, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<VehicleModel>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/model/{id}")
    public ResponseEntity<?> deleteModel(@PathVariable("id")long id){
        try {
            VehicleModel vehiclemodel = this.vehiclemodelrepo.findById(id).orElse(null);
            if(vehiclemodel!= null){
                this.vehiclemodelrepo.deleteById(id);
                return new ResponseEntity<VehicleModel>(vehiclemodel, HttpStatus.OK);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Brand not found");
            }

        } catch (Exception e) {
            return new ResponseEntity<VehicleModel>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/ModelByType/{typeId}")
    public ResponseEntity<List<VehicleModel>> getModelByType(
        @PathVariable("typeId") Long typId
    ){
        try {
            List<VehicleModel> variant = this.vehiclemodelrepo.findByTypeId(typId);
            return new ResponseEntity<List<VehicleModel>>(variant, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<VehicleModel>>(HttpStatus.NO_CONTENT);
        }
    }
}
