package com.test.seru.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")

public class CarRestController {
    @GetMapping(value = "cars")
    @ResponseBody
    public String cars(){
        String car = "SUV";
        return car;
    }
}
