package com.test.seru.controllers;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.seru.models.PriceList;
import com.test.seru.repositories.PriceListRepo;


@RestController
@RequestMapping(value = "/api")
@CrossOrigin("*")
public class PriceListRestController {

     @Autowired private PriceListRepo pricerepo;

    @GetMapping("/price")
    public ResponseEntity<List<PriceList>> getAllPrice(){
        try {
            List<PriceList> price = this.pricerepo.getAllPriceList();
            return new ResponseEntity<List<PriceList>>(price, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<PriceList>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("price/{id}")
    public ResponseEntity<?> getPriceById(@PathVariable("id") Long id){
        try {
            PriceList price = this.pricerepo.findById(id).orElse(null);
            if(price != null){
                return new ResponseEntity<PriceList>(price, HttpStatus.OK);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Price List not found");
            }

        } catch (Exception e) {
            return new ResponseEntity<PriceList>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/price")
    public ResponseEntity<PriceList> insertPriceList(@RequestBody PriceList price){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            price.setCreated_on(timestamp);
            price.setIs_delete(false);
            this.pricerepo.save(price);
            return new ResponseEntity<PriceList>(price, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<PriceList>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/editprice{id}")
    public ResponseEntity<PriceList> editPriceListById(@RequestBody PriceList price, @PathVariable("id") Long id){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            price.setId(id);
            price.setUpdated_on(timestamp);
            price.setIs_delete(false);
            this.pricerepo.save(price);
            return new ResponseEntity<PriceList>(price, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<PriceList>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/price/{id}")
    public ResponseEntity<?> deletePriceList(@PathVariable("id")long id){
        try {
            PriceList price = this.pricerepo.findById(id).orElse(null);
            if(price!= null){
                this.pricerepo.deleteById(id);
                return new ResponseEntity<PriceList>(price, HttpStatus.OK);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("PriceList not found");
            }

        } catch (Exception e) {
            return new ResponseEntity<PriceList>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/PriceByYear/{yearId}")
    public ResponseEntity<List<PriceList>> getPriceListByYear(
        @PathVariable("yearId") Long yrId
    ){
        try {
            List<PriceList> price = this.pricerepo.findByYearId(yrId);
            return new ResponseEntity<List<PriceList>>(price, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<PriceList>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/PriceByModel/{modelId}")
    public ResponseEntity<List<PriceList>> getPriceListByModel(
        @PathVariable("modelId") Long mdId
    ){
        try {
            List<PriceList> price = this.pricerepo.findByYearId(mdId);
            return new ResponseEntity<List<PriceList>>(price, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<PriceList>>(HttpStatus.NO_CONTENT);
        }
    }
    
}
