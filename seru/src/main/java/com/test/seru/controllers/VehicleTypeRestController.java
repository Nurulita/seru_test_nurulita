package com.test.seru.controllers;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.seru.models.VehicleType;
import com.test.seru.repositories.VehicleTypeRepo;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin("*")
public class VehicleTypeRestController {
    
    @Autowired private VehicleTypeRepo vehicletyperepo;

    @GetMapping("/type")
    public ResponseEntity<List<VehicleType>> getAlltype(){
        try {
            List<VehicleType> vehicletype = this.vehicletyperepo.getAllType();
            return new ResponseEntity<List<VehicleType>>(vehicletype, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<VehicleType>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("type/{id}")
    public ResponseEntity<?> getTypeById(@PathVariable("id") Long id){
        try {
            VehicleType vehicletype = this.vehicletyperepo.findById(id).orElse(null);
            if(vehicletype != null){
                return new ResponseEntity<VehicleType>(vehicletype, HttpStatus.OK);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Brand not found");
            }

        } catch (Exception e) {
            return new ResponseEntity<VehicleType>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/type")
    public ResponseEntity<VehicleType> insertType(@RequestBody VehicleType vehicletype){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            vehicletype.setCreated_on(timestamp);
            vehicletype.setIs_delete(false);
            this.vehicletyperepo.save(vehicletype);
            return new ResponseEntity<VehicleType>(vehicletype, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<VehicleType>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/edittype{id}")
    public ResponseEntity<VehicleType> editTypeById(@RequestBody VehicleType type, @PathVariable("id") Long id){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            type.setId(id);
            type.setUpdated_on(timestamp);
            type.setIs_delete(false);
            this.vehicletyperepo.save(type);
            return new ResponseEntity<VehicleType>(type, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<VehicleType>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/type/{id}")
    public ResponseEntity<?> deleteType(@PathVariable("id")long id){
        try {
            VehicleType vehicletype = this.vehicletyperepo.findById(id).orElse(null);
            if(vehicletype!= null){
                this.vehicletyperepo.deleteById(id);
                return new ResponseEntity<VehicleType>(vehicletype, HttpStatus.OK);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Type not found");
            }

        } catch (Exception e) {
            return new ResponseEntity<VehicleType>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/TypeByBrand/{brandId}")
    public ResponseEntity<List<VehicleType>> getTypeByBrand(
        @PathVariable("brandId") Long brnId
    ){
        try {
            List<VehicleType> type = this.vehicletyperepo.findByBrandId(brnId);
            return new ResponseEntity<List<VehicleType>>(type, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<VehicleType>>(HttpStatus.NO_CONTENT);
        }
    }

}
