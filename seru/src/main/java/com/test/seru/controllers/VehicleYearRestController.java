package com.test.seru.controllers;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.seru.models.VehicleYear;
import com.test.seru.repositories.VehicleYearRepo;


@RestController
@RequestMapping(value = "/api")
@CrossOrigin("*")
public class VehicleYearRestController {
    @Autowired private VehicleYearRepo vehicleyearrepo;

    @GetMapping("/year")
    public ResponseEntity<List<VehicleYear>> getAllYear(){
        try {
            List<VehicleYear> vehicleyear = this.vehicleyearrepo.getAllYear();
            return new ResponseEntity<List<VehicleYear>>(vehicleyear, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<VehicleYear>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("year/{id}")
    public ResponseEntity<?> getyearById(@PathVariable("id") Long id){
        try {
            VehicleYear vehicleyear = this.vehicleyearrepo.findById(id).orElse(null);
            if(vehicleyear!= null){
                return new ResponseEntity<VehicleYear>(vehicleyear, HttpStatus.OK);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Year not found");
            }

        } catch (Exception e) {
            return new ResponseEntity<VehicleYear>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/year")
    public ResponseEntity<VehicleYear> insertYear(@RequestBody VehicleYear vehicleyear){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            vehicleyear.setCreated_on(timestamp);
            vehicleyear.setIs_delete(false);
            this.vehicleyearrepo.save(vehicleyear);
            return new ResponseEntity<VehicleYear>(vehicleyear, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<VehicleYear>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/edityear{id}")
    public ResponseEntity<VehicleYear> editTypeById(@RequestBody VehicleYear year, @PathVariable("id") Long id){
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            year.setId(id);
            year.setUpdated_on(timestamp);
            year.setIs_delete(false);
            this.vehicleyearrepo.save(year);
            return new ResponseEntity<VehicleYear>(year, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<VehicleYear>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/year/{id}")
    public ResponseEntity<?> deletYear(@PathVariable("id")long id){
        try {
            VehicleYear vehicleyear = this.vehicleyearrepo.findById(id).orElse(null);
            if(vehicleyear!= null){
                this.vehicleyearrepo.deleteById(id);
                return new ResponseEntity<VehicleYear>(vehicleyear, HttpStatus.OK);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Year not found");
            }

        } catch (Exception e) {
            return new ResponseEntity<VehicleYear>(HttpStatus.NO_CONTENT);
        }
    }
}
