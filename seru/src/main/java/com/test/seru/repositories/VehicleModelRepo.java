package com.test.seru.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.seru.models.VehicleModel;

@Repository
public interface VehicleModelRepo extends JpaRepository<VehicleModel, Long>{
    @Query(value = "SELECT * FROM VehicleModel WHERE is_delete = false", nativeQuery = true)
    List<VehicleModel>getAllModel();

    @Query(value = "SELECT m FROM VehicleModel m WHERE m.Type_id = ?1")
    List<VehicleModel> findByTypeId(Long typeId);
}
