package com.test.seru.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.seru.models.PriceList;

@Repository
public interface PriceListRepo extends JpaRepository<PriceList, Long>{
    @Query(value = "SELECT * FROM PriceList WHERE is_delete = false", nativeQuery = true)
    List<PriceList>getAllPriceList();

    @Query(value = "SELECT p FROM PriceList p WHERE p.Model_id = ?1")
    List<PriceList> findByModelId(Long modelId);
    @Query(value = "SELECT p FROM PriceList p WHERE p.Year_id = ?1")
    List<PriceList> findByYearId(Long yearId);
       
}
