package com.test.seru.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.seru.models.VehicleModel;
import com.test.seru.models.VehicleType;

@Repository
public interface VehicleTypeRepo extends JpaRepository<VehicleType, Long>{
    @Query(value = "SELECT * FROM VehicleType WHERE is_delete = false", nativeQuery = true)
    List<VehicleType>getAllType();

    @Query(value = "SELECT t FROM VehicleType t WHERE t.Brand_id = ?1")
    List<VehicleType> findByBrandId(Long brandId);
}
