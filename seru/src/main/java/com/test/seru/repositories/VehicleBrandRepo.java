package com.test.seru.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.seru.models.VehicleBrand;

@Repository
public interface VehicleBrandRepo extends JpaRepository<VehicleBrand, Long>{
    @Query(value = "SELECT * FROM VehicleBrand WHERE is_delete = false", nativeQuery = true)
    List<VehicleBrand> getAllBrand();
    @Query(value = "SELECT * FROM VehicleBrand WHERE name ILIKE ?1 AND is_delete = false", nativeQuery = true)
    List<VehicleBrand> getbrandByName(String name);
    
}
