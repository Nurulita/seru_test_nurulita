package com.test.seru.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.seru.models.VehicleYear;

@Repository
public interface VehicleYearRepo extends JpaRepository<VehicleYear, Long>{
    @Query(value = "SELECT * FROM VehicleYear WHERE is_delete = false", nativeQuery = true)
    List<VehicleYear>getAllYear();
}
