package com.test.seru.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.seru.models.User;

@Repository
public interface UserRepo {
    @Query(value = "SELECT * FROM User WHERE is_delete = false", nativeQuery = true)
    List<User> getAllUser();
}
