package com.test.seru.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "vehicle_type")
public class VehicleType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "name", length = 100)
    private String Name;

    @ManyToOne
	@JoinColumn(name="brand_id", insertable=false, updatable=false)
	public VehicleBrand vehicle_brand;
	
	@NotNull
	@Column(name="brand_id")
	private Long Brand_id;

    @NotNull
    @Column(name="created_by")
    private Long Created_by;

    @NotNull
    @Column(name="created_on")
    private Timestamp Created_on;

    @Nullable
    @Column(name="updated_by")
    private Long Updated_by;

    @Nullable
    @Column(name="updated_on")
    private Timestamp Updated_on;
    
    @Nullable
    @Column(name="deleted_by")
    private Long Deleted_by;

    @Nullable
    @Column(name="deleted_on")
    private Timestamp Deleted_on;

    @NotNull
    @Column(name="is_delete",columnDefinition = "boolean default false")
    private Boolean Is_delete;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public VehicleBrand getVehicle_brand() {
        return vehicle_brand;
    }

    public void setVehicle_brand(VehicleBrand vehicle_brand) {
        this.vehicle_brand = vehicle_brand;
    }

    public Long getBrand_id() {
        return Brand_id;
    }

    public void setBrand_id(Long brand_id) {
        Brand_id = brand_id;
    }

    public Long getCreated_by() {
        return Created_by;
    }

    public void setCreated_by(Long created_by) {
        Created_by = created_by;
    }

    public Timestamp getCreated_on() {
        return Created_on;
    }

    public void setCreated_on(Timestamp created_on) {
        Created_on = created_on;
    }

    public Long getUpdated_by() {
        return Updated_by;
    }

    public void setUpdated_by(Long updated_by) {
        Updated_by = updated_by;
    }

    public Timestamp getUpdated_on() {
        return Updated_on;
    }

    public void setUpdated_on(Timestamp updated_on) {
        Updated_on = updated_on;
    }

    public Long getDeleted_by() {
        return Deleted_by;
    }

    public void setDeleted_by(Long deleted_by) {
        Deleted_by = deleted_by;
    }

    public Timestamp getDeleted_on() {
        return Deleted_on;
    }

    public void setDeleted_on(Timestamp deleted_on) {
        Deleted_on = deleted_on;
    }

    public Boolean getIs_delete() {
        return Is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        Is_delete = is_delete;
    }
}
